public class Fibonacci {

    public static void main(String[] args) {
        int count = 10;

        //print first 10 fibonacci numbers
        int initializer1 = 0;
        int initializer2 = 1;
        int next = 0;

        System.out.print(initializer1 + " "+initializer2);
        for (int i = 0; i < count -2; i++) //count -2 because two numbers act as initializers in the series
        {
            next = initializer1 + initializer2;
            System.out.print(" "+ next);
            initializer1 = initializer2;
            initializer2 = next;
        }
    }
}
